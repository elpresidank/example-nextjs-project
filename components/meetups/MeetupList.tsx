import MeetupItem from './MeetupItem';
import classes from './MeetupList.module.scss';

type Meetup = {
  id: string;
  image: string;
  title: string;
  address: string;
}

interface Iprops {
  meetups: Meetup[];
}

function MeetupList(props: Iprops) {
  return (
    <ul className={classes.list}>
      {props.meetups.map((meetup: Meetup) => (
        <MeetupItem
          key={meetup.id}
          id={meetup.id}
          image={meetup.image}
          title={meetup.title}
          address={meetup.address}
        />
      ))}
    </ul>
  );
}

export default MeetupList;