import MainNavigation from './MainNavigation';
import classes from './Layout.module.scss';
import React from 'react';
import {FC} from 'react';

const Layout: FC = (props) => {
  return (
    <div>
      <MainNavigation />
      <main className={classes.main}>{props.children}</main>
    </div>
  );
}

export default Layout;