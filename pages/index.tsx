import MeetupList from '../components/meetups/MeetupList';

type Meetup = {
  id: string;
  title: string;
  image: string;
  address: string;
  description: string;
}

interface Iprops {
  meetups: Array<Meetup>
}

const DUMMY_MEETUPS = [
  {
    id: 'm1',
    title: 'A First Meetup',
    image: 'https://upload.wikimedia.org/wikipedia/commons/d/d3/Stadtbild_M%C3%BCnchen.jpg',
    address: 'Some address 5, 12345 Some City',
    description: 'This is a first meetup!',
  },
  {
    id: 'm2',
    title: 'A Second Meetup',
    image: 'https://upload.wikimedia.org/wikipedia/commons/d/d3/Stadtbild_M%C3%BCnchen.jpg',
    address: 'Some address 5, 125465 Some City',
    description: 'This is a second meetup!',
  },
];

function HomePage(props: Iprops) {

  return (
    <MeetupList meetups={props.meetups} />
  );
}

// use getStaticProps for pages with data that does not change frequently (i.e menu items)
export async function getStaticProps() {
// fetch data from an API
  return {
    props: {
      meetups: DUMMY_MEETUPS,
    },
    revalidate: 10, // Use higher number for data that is changed frequently
  };
}

// Will re-render page each time requested on the server side
// export async function getServerSideProps(context: any) {
//   const req = context.req;
//   const res = context.res;
//
//   // fetch data from an API
//
//   return {
//     props: {
//       meetups: DUMMY_MEETUPS,
//     },
//   };
// }

export default HomePage;